import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Qs from 'qs';

const RouterWrapper = Inner =>
  class extends Component {
    static contextTypes = {
      router: PropTypes.object,
    };

    static displayName = `RouterWrapper(${Inner.displayName || Inner.name})`;

    router = {
      getCurrentQuery: () =>
        Qs.parse(this.context.router.history.location.search.substring(1)),
      getCurrentPath: () => this.context.router.history.location.pathname,
      navigateTo: (path, query) =>
        this.context.router.history.push(`${path}?${Qs.stringify(query)}`),
    };

    render() {
      return <Inner {...this.props} router={this.router} />;
    }
  };

export default RouterWrapper;
