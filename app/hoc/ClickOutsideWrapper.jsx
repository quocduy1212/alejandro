import _ from 'lodash';
import React, { Component } from 'react';

const ClickOutsideWrapper = Inner =>
  class extends Component {
    static displayName = `ClickOutsideWrapper(${Inner.displayName ||
      Inner.name})`;

    uniqueId = _.uniqueId('click_outside_id_');

    componentDidMount() {
      document.addEventListener('click', this.onClickOutside);
    }

    componentWillUnmount() {
      document.removeEventListener('click', this.onClickOutside);
    }

    onClickOutside = event => {
      if (!event.target.closest(`div[id=${this.uniqueId}]`)) {
        if (this.innerRef.onClickOutside) {
          this.innerRef.onClickOutside();
        }
      }
    };

    render() {
      return (
        <div id={this.uniqueId}>
          <Inner
            {...this.props}
            ref={ref => {
              this.innerRef = ref;
            }}
          />
        </div>
      );
    }
  };

export default ClickOutsideWrapper;
