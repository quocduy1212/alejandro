/* eslint-disable global-require */
import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { AppContainer } from 'react-hot-loader';

import 'app-vendor';
import 'app-styles/main';

import store from './store/configure-store';
import Root from './root';

render(
  <AppContainer>
    <Root store={store} />
  </AppContainer>,
  document.getElementById('phamduy-container')
);

if (module.hot) {
  module.hot.accept('./root', () => {
    const NewRoot = require('./root').default;
    render(
      <AppContainer>
        <NewRoot store={store} />
      </AppContainer>,
      document.getElementById('phamduy-container')
    );
  });
}
