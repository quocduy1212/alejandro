import { takeLatest } from 'redux-saga/effects';
import TYPES from 'app-types';
import { createSagaForAsyncType } from 'app-libs/utilsAsync';
import * as apis from 'app-libs/api';

export default function* asyncSaga() {
  yield takeLatest(
    TYPES.FETCH_USERS,
    createSagaForAsyncType(TYPES.ASYNC_TYPES.FETCH_USERS, apis.fetchUsers)
  );
  yield takeLatest(
    TYPES.FETCH_CONFIG,
    createSagaForAsyncType(TYPES.ASYNC_TYPES.FETCH_CONFIG, apis.fetchConfig)
  );
}
