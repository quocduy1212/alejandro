import { fork } from 'redux-saga/effects';
import asyncSaga from './async';

function* mainSaga() {
  yield fork(asyncSaga);
}

export default mainSaga;
