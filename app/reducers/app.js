import TYPES from 'app-types';
import { createAsyncHanders } from 'app-libs/utilsAsync';
import { fromJS } from 'immutable';

const DEFAULT_STATE = fromJS({
  filters: { name: '' },
  users: {
    api: {},
  },
  config: {
    api: {},
  },
});

const handlers = {
  [TYPES.FILTERS]: (state, action) =>
    state.mergeIn(['filters'], action.filters),
  ...createAsyncHanders(TYPES.ASYNC_TYPES),
};

const app = (state = DEFAULT_STATE, action) =>
  handlers[action.type] ? handlers[action.type](state, action) : state;

export default app;
