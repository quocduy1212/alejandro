export const HOME = {
  href: '/app/home',
  label: 'Home',
  path: '/app/home',
  key: 'home',
};

export const DISTRIBUTION = {
  href: '/app/distribution',
  path: '/app/distribution',
  label: 'Email Distribution',
  key: 'distribution',
};

export const MAP = {
  href: '/app/map',
  path: '/app/map',
  label: 'Contact Map',
  key: 'map',
};

export const MENUS = [HOME, DISTRIBUTION, MAP];
