import { createAsyncTypes } from 'app-libs/utilsAsync';

const FETCH_USERS = 'FETCH_USERS';
const FETCH_CONFIG = 'FETCH_CONFIG';
const FILTERS = 'FILTERS';

export default {
  FETCH_USERS,
  FETCH_CONFIG,
  FILTERS,
  ...createAsyncTypes([
    [FETCH_USERS, ['users', 'api']],
    [FETCH_CONFIG, ['config', 'api']],
  ]),
};
