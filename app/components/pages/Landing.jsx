import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect, Switch } from 'react-router-dom';
import { createMenu } from 'app-libs/utilsMenu';
import { connect } from 'react-redux';
import * as appActions from 'app-actions/app';
import RouterWrapper from 'app-hoc/RouterWrapper';
import { NavMenu, TopBar, Left } from '../common';
import Home from './Home';
import AddressMap from './Map';
import EmailDistribution from './Distribution';

class Landing extends Component {
  componentDidMount() {
    this.props.fetchUsers();
  }

  onMenuSelect = url => this.props.router.navigateTo(url);

  render() {
    const { location } = this.props;
    return (
      <Fragment>
        <TopBar>
          <Left>
            <NavMenu
              menus={createMenu(location.pathname)}
              onMenuSelect={this.onMenuSelect}
            />
          </Left>
        </TopBar>
        <Switch>
          <Redirect exact from="/app" to="/app/home" />
          <Route path="/app/home" component={Home} />
          <Route path="/app/distribution" component={EmailDistribution} />
          <Route path="/app/map" component={AddressMap} />
        </Switch>
      </Fragment>
    );
  }
}

Landing.propTypes = {
  fetchUsers: PropTypes.func.isRequired,
  location: PropTypes.object.isRequired,
  router: PropTypes.object.isRequired,
};

export default connect(null, {
  fetchUsers: appActions.fetchUsers,
})(RouterWrapper(Landing));
