import React, { Component } from 'react';
import { Redirect, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as appSelectors from 'app-selectors/app';

class Redirector extends Component {
  static propTypes = {
    config: PropTypes.object.isRequired,
    children: PropTypes.node,
  };

  static defaultProps = {
    children: null,
  };

  componentDidMount() {
    /* eslint-disable no-console */
    console.log('permission/role check...');
    console.log('good to go');
  }

  isGranted = () => this.props.config.authenticated;

  render() {
    return this.isGranted() ? (
      this.props.children
    ) : (
      <Redirect to="/somewhere-else" />
    );
  }
}

const mapStateToProps = state => ({
  config: appSelectors.fetchConfigData(state),
});

export default withRouter(connect(mapStateToProps, null)(Redirector));
