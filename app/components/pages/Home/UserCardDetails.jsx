import React from 'react';
import PropTypes from 'prop-types';
import { randomColor } from 'app-libs/utilsColor';

const randomBackground = () => ({
  background: randomColor(),
});

const UserCardDetails = ({ name, website, email, phone, company }) => (
  <div className="w-100 h-100 tr pa4">
    <div className="absolute w2 w3-ns h-100 top-0" style={randomBackground()} />
    <div className="f3 fw6">{name}</div>
    <div className="f5">{phone}</div>
    <a href={`mailto:${email}`} className="f5 bright-blue db">
      {email}
    </a>
    <a href={`http://${website}`} className="f5 bright-blue db">
      {website}
    </a>

    <div className="f4 mt4">{company.name}</div>
    <div className="f6">{company.catchPhrase}</div>
    <div className="f6">{company.bs}</div>
  </div>
);
UserCardDetails.propTypes = {
  name: PropTypes.string,
  website: PropTypes.string,
  email: PropTypes.string,
  phone: PropTypes.string,
  company: PropTypes.object,
};

UserCardDetails.defaultProps = {
  name: '',
  website: '',
  email: '',
  phone: '',
  company: '',
};

export default UserCardDetails;
