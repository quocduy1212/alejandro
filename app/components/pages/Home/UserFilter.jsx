import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { TextInput } from 'app-comps/common';

const UserFilter = props => (
  <div className={classNames('relative', props.className)}>
    <i className="material-icons absolute mt1">search</i>
    <TextInput
      placeholder={props.placeholder}
      value={props.value}
      onChange={props.onChange}
      className="ph4"
    />
  </div>
);

UserFilter.propTypes = {
  className: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
};

UserFilter.defaultProps = {
  className: '',
  placeholder: '',
  value: '',
  onChange: _.noop,
};

export default UserFilter;
