import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { HighlightableText } from 'app-comps/common';

const UserCard = ({
  name,
  website,
  className,
  highlight,
  id,
  onSelectUser,
}) => {
  const containerSmall = classnames('w-100');
  const containerMedium = classnames('w-third-m');
  const containerLarge = classnames('w-25-l');
  const containerCls = classnames(
    'fl pa3 pointer',
    className,
    containerSmall,
    containerMedium,
    containerLarge
  );
  return (
    <div className={containerCls} onClick={() => onSelectUser(id)}>
      <HighlightableText
        className="f4 bright-blue"
        text={name}
        highlight={highlight}
      />
      <div className="f6">{website}</div>
    </div>
  );
};

UserCard.propTypes = {
  className: PropTypes.string,
  name: PropTypes.string,
  highlight: PropTypes.string,
  website: PropTypes.string,
  id: PropTypes.number,
  onSelectUser: PropTypes.func,
};

UserCard.defaultProps = {
  className: '',
  name: '',
  highlight: '',
  website: '',
  id: null,
  onSelectUser: _.noop,
};

export default UserCard;
