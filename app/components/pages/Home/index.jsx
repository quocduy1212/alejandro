import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as appSelectors from 'app-selectors/app';
import * as appActions from 'app-actions/app';
import RouterWrapper from 'app-hoc/RouterWrapper';
import { Loader, Modal, ModalBody, MainContainer } from 'app-comps/common';
import UserCard from './UserCard';
import UserCardDetails from './UserCardDetails';
import UserFilter from './UserFilter';

class Home extends Component {
  static propTypes = {
    users: PropTypes.array,
    usersStatus: PropTypes.object,
    router: PropTypes.object.isRequired,
    filters: PropTypes.object,
    filterByName: PropTypes.func.isRequired,
  };

  static contextTypes = {
    router: PropTypes.object,
  };

  static defaultProps = {
    users: [],
    usersStatus: {},
    filters: {},
  };

  componentDidMount() {
    const query = this.props.router.getCurrentQuery();
    this.props.filterByName(query.name || '');
  }

  onCloseUserDetailsModal = () => {
    this.navigateTo({ userId: '' });
  };

  onSelectUser = userId => {
    this.navigateTo({ userId });
  };

  onSearch = event => {
    const name = event.target.value;
    this.props.filterByName(name);
    this.navigateTo({ name });
  };

  navigateTo = newQuery => {
    const query = this.props.router.getCurrentQuery();
    const path = this.props.router.getCurrentPath();
    this.props.router.navigateTo(path, { ...query, ...newQuery });
  };

  getSelectedUser = () => {
    const { users, router } = this.props;
    const { userId } = router.getCurrentQuery();
    if (userId) {
      return users.find(u => `${u.id}` === userId);
    }
    return undefined;
  };

  render() {
    const { users, usersStatus, filters } = this.props;
    const selectedUser = this.getSelectedUser();
    return (
      <MainContainer>
        {usersStatus.isLoading && <Loader message="loading users..." />}
        {usersStatus.isSuccess && (
          <Fragment>
            {selectedUser && (
              <Modal
                className="animated fadeIn"
                onClose={this.onCloseUserDetailsModal}
              >
                <ModalBody>
                  <UserCardDetails {...selectedUser} />
                </ModalBody>
              </Modal>
            )}
            <UserFilter
              className="ml3"
              placeholder="Filter user by name"
              onChange={this.onSearch}
              value={filters.name}
            />
            <div className="cf">
              {users.map(u => (
                <UserCard
                  key={u.id}
                  id={u.id}
                  name={u.name}
                  website={u.website}
                  highlight={filters.name}
                  onSelectUser={this.onSelectUser}
                />
              ))}
            </div>
          </Fragment>
        )}
      </MainContainer>
    );
  }
}

const mapStateToProps = state => ({
  users: appSelectors.getFilteredUsers(state),
  usersStatus: appSelectors.fetchUsersStatus(state),
  filters: appSelectors.getFilters(state),
});

export default connect(mapStateToProps, {
  filterByName: appActions.filterByName,
})(RouterWrapper(Home));
