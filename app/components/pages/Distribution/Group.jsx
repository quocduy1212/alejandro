import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Badge } from 'app-comps/common';

const Group = ({ className, char, items }) => {
  const groupCls = classnames('w3 h3 ma2', className);
  return (
    <Badge
      className={groupCls}
      text={char}
      badge={`${items.length}`}
      disable={!items.length}
    />
  );
};

Group.propTypes = {
  className: PropTypes.string,
  items: PropTypes.array,
  char: PropTypes.string,
};

Group.defaultProps = {
  className: '',
  items: [],
  char: '',
};

export default Group;
