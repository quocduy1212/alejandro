import _ from 'lodash';
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Loader, MainContainer } from 'app-comps/common';
import * as appSelectors from 'app-selectors/app';
import Group from './Group';

const ALPHABET_COUNT = 25;
const CHAR_a_CODE = 97;
const CHAR_A_CODE = 65;

const EmailDistribution = ({ distribution, usersStatus }) => (
  <MainContainer>
    {usersStatus.isLoading && <Loader message="loading users..." />}
    {usersStatus.isSuccess && (
      <Fragment>
        {_.times(ALPHABET_COUNT).map(n => {
          const char = String.fromCharCode(CHAR_A_CODE + n);
          return <Group key={char} char={char} items={distribution[char]} />;
        })}
        {_.times(ALPHABET_COUNT).map(n => {
          const char = String.fromCharCode(CHAR_a_CODE + n);
          return <Group key={char} char={char} items={distribution[char]} />;
        })}
      </Fragment>
    )}
  </MainContainer>
);

EmailDistribution.propTypes = {
  distribution: PropTypes.object,
  usersStatus: PropTypes.object,
};

EmailDistribution.defaultProps = {
  distribution: {},
  usersStatus: {},
};

const mapStateToProps = state => ({
  usersStatus: appSelectors.fetchUsersStatus(state),
  distribution: appSelectors.getEmailDistribution(state),
});

export default connect(mapStateToProps, {})(EmailDistribution);
