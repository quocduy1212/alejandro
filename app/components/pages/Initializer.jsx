import React, { Component, Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Loader } from 'app-comps/common';
import * as appActions from 'app-actions/app';
import * as appSelectors from 'app-selectors/app';

class Initializer extends Component {
  static propTypes = {
    children: PropTypes.node,
    fetchConfig: PropTypes.func.isRequired,
    configStatus: PropTypes.object.isRequired,
  };

  static defaultProps = {
    children: null,
  };

  componentDidMount() {
    this.props.fetchConfig();
  }

  isConfigLoaded = () => true;

  render() {
    const { configStatus, children } = this.props;
    return (
      <Fragment>
        {configStatus.isLoading && (
          <Loader message="loading application config..." />
        )}
        {configStatus.isSuccess && children}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  configStatus: appSelectors.fetchConfigStatus(state),
});

export default withRouter(
  connect(mapStateToProps, {
    fetchConfig: appActions.fetchConfig,
  })(Initializer)
);
