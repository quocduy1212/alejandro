import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Loader, MainContainer } from 'app-comps/common';
import * as appSelectors from 'app-selectors/app';
import Map from './Map';

const AddressMap = ({ addresses, usersStatus }) => (
  <MainContainer>
    {usersStatus.isLoading && <Loader message="loading users..." />}
    {usersStatus.isSuccess && <Map addresses={addresses} />}
  </MainContainer>
);

AddressMap.propTypes = {
  addresses: PropTypes.array,
  usersStatus: PropTypes.object,
};

AddressMap.defaultProps = {
  addresses: [],
  usersStatus: {},
};

const mapStateToProps = state => ({
  addresses: appSelectors.getUsersAddress(state),
  usersStatus: appSelectors.fetchUsersStatus(state),
});

export default connect(mapStateToProps, {})(AddressMap);
