import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { DEV_API_KEY } from 'app-constants/appConfig';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
} from 'react-google-maps';

class Map extends Component {
  static propTypes = {
    addresses: PropTypes.array,
  };

  static defaultProps = {
    addresses: [],
  };

  fitBounds = () => {
    const { LatLng, LatLngBounds } = window.google.maps;
    const latlng = this.props.addresses.map(
      addr => new LatLng(addr.geo.lat, addr.geo.lng)
    );
    const bounds = new LatLngBounds();
    latlng.forEach(ll => {
      bounds.extend(ll);
    });
    this.mapRef.fitBounds(bounds);
  };

  render() {
    const { addresses } = this.props;
    return (
      <GoogleMap
        ref={ref => {
          this.mapRef = ref;
        }}
        onIdle={() => {
          this.fitBounds();
          this.fitBounds = _.noop;
        }}
      >
        {addresses.map(addr => <Marker key={addr.id} position={addr.geo} />)}
      </GoogleMap>
    );
  }
}
const WrappedMap = withScriptjs(withGoogleMap(Map));

/* eslint-disable react/prop-types */
export default ({ addresses }) => (
  <WrappedMap
    googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&key=${DEV_API_KEY}&libraries=geometry`}
    loadingElement={<div style={{ height: '100%' }} />}
    containerElement={<div style={{ height: '100%' }} />}
    mapElement={<div style={{ height: '100%' }} />}
    addresses={addresses}
  />
);
