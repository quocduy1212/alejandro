import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './styles.scss';

const Badge = ({ text, badge, className, disable }) => {
  const badgeCls = classnames('badge dib', className);
  const badgeCountCls = classnames('badge-count f5 fw6', { disable });
  return (
    <div className={badgeCls}>
      <span className="f2 fw6">{text}</span>
      <span className={badgeCountCls}>{badge}</span>
    </div>
  );
};

Badge.propTypes = {
  className: PropTypes.string,
  text: PropTypes.string,
  badge: PropTypes.string,
  disable: PropTypes.bool,
};

Badge.defaultProps = {
  className: '',
  text: '',
  badge: '',
  disable: false,
};

export default Badge;
