import React, { Fragment } from 'react';
import ResponsiveMenu from './ResponsiveMenu';
import HorizontalMenu from './HorizontalMenu';

import './style.scss';

const NavMenu = props => (
  <Fragment>
    <HorizontalMenu {...props} />
    <ResponsiveMenu {...props} />
  </Fragment>
);

export default NavMenu;
