import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import MenuItem from './MenuItem';

const HorizontalMenu = ({ menus, className, align, onMenuSelect }) => {
  const containerCls = classnames(
    'nav-menu-container',
    'dn db-m db-l',
    className
  );
  const menusCls = classnames('nav-menu', 'pa0 ma0');
  const menuItemCls = classnames(
    'nav-menu-item',
    'dib',
    align === 'left' ? 'mr3' : 'ml3'
  );

  return (
    <div className={containerCls}>
      <ul className={menusCls}>
        {menus.map(m => (
          <MenuItem
            key={m.key}
            label={m.label}
            href={m.href}
            active={m.active}
            className={classnames(m.className, menuItemCls)}
            onMenuSelect={onMenuSelect}
          />
        ))}
      </ul>
    </div>
  );
};

HorizontalMenu.propTypes = {
  menus: PropTypes.array,
  className: PropTypes.string,
  align: PropTypes.oneOf(['right', 'left']),
  onMenuSelect: PropTypes.func,
};

HorizontalMenu.defaultProps = {
  menus: [],
  className: '',
  align: 'left',
  onMenuSelect: _.noop,
};

export default HorizontalMenu;
