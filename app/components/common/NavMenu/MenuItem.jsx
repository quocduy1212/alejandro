import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const MenuItem = ({ href, label, className, active, onMenuSelect }) => (
  <li className={classnames('nav-menu-item', className)}>
    <a
      className={classnames({ active })}
      href={href}
      onClick={event => {
        event.preventDefault();
        onMenuSelect(href);
      }}
    >
      {label}
    </a>
  </li>
);

MenuItem.propTypes = {
  href: PropTypes.string,
  className: PropTypes.string,
  label: PropTypes.string,
  active: PropTypes.bool,
  onMenuSelect: PropTypes.func,
};

MenuItem.defaultProps = {
  href: '',
  className: '',
  label: '',
  active: false,
  onMenuSelect: _.noop,
};

export default MenuItem;
