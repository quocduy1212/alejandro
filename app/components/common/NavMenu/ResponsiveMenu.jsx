import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import MenuItem from './MenuItem';

class ResponsiveMenu extends Component {
  static propTypes = {
    menus: PropTypes.array,
    className: PropTypes.string,
    onMenuSelect: PropTypes.func,
  };

  static defaultProps = {
    menus: [],
    className: '',
    onMenuSelect: _.noop,
  };

  state = {
    expanded: false,
  };

  onMenuToggle = () => {
    this.setState(prevState => ({ expanded: !prevState.expanded }));
  };

  onMenuSelect = href => {
    this.onMenuToggle();
    this.props.onMenuSelect(href);
  };

  render() {
    const { menus, className } = this.props;
    const { expanded } = this.state;
    const containerCls = classnames('nav-menu-container', 'dn-ns', className);
    const iconCls = classnames('material-icons', 'pointer');
    const menusCls = classnames(
      'nav-menu-responsive',
      'ph5 fixed left-0 right-0 z-max',
      { expanded }
    );
    const menuItemCls = classnames('db mt3');

    return (
      <div className={containerCls}>
        <i className={iconCls} onClick={this.onMenuToggle}>
          menu
        </i>
        <ul className={menusCls}>
          {menus.map(m => (
            <MenuItem
              key={m.key}
              label={m.label}
              href={m.href}
              active={m.active}
              className={classnames(m.className, menuItemCls)}
              onMenuSelect={this.onMenuSelect}
            />
          ))}
        </ul>
      </div>
    );
  }
}

export default ResponsiveMenu;
