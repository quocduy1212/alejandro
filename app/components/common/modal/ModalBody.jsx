import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const ModalBody = ({ children, className }) => (
  <div className={classnames('modal-body', className)}>{children}</div>
);

ModalBody.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};

ModalBody.defaultProps = {
  children: null,
  className: '',
};

export default ModalBody;
