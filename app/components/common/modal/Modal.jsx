import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import ClickOutsideWrapper from 'app-hoc/ClickOutsideWrapper';
import Backdrop from '../layout/Backdrop';

import './styles.scss';

class Modal extends Component {
  onClickOutside = () => this.props.onClose();

  render() {
    const { className, children } = this.props;
    const containerSmall = classnames('w-100');
    const containerMedium = classnames('w-80-m');
    const containerLarge = classnames('w-50-l');
    const containerCls = classnames(
      'modal bg-white pa2',
      className,
      containerSmall,
      containerMedium,
      containerLarge
    );
    return (
      <div className={containerCls}>
        <i
          className="material-icons absolute right-1 top-1 pointer f4 nt1 nr1"
          onClick={this.props.onClose}
        >
          clear
        </i>
        {children}
      </div>
    );
  }
}

Modal.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  onClose: PropTypes.func,
};

Modal.defaultProps = {
  className: '',
  children: null,
  onClose: _.noop,
};

const ClickOutsideWrapped = ClickOutsideWrapper(Modal);

const BackdropWrapped = props => (
  <Backdrop>
    <ClickOutsideWrapped {...props} />
  </Backdrop>
);

BackdropWrapped.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
};

BackdropWrapped.defaultProps = {
  className: '',
  children: null,
};

export default BackdropWrapped;
