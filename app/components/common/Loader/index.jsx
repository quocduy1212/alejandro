import React from 'react';
import PropTypes from 'prop-types';

import './styles.scss';

const Loader = ({ message }) => <div className="loader">{message}</div>;

Loader.propTypes = {
  message: PropTypes.string,
};

Loader.defaultProps = {
  message: '',
};

export default Loader;
