import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const createMarkup = (text, highlight) => ({
  __html: text.replace(highlight, `<mark>${highlight}</mark>`),
});

/* eslint-disable react/no-danger */
const HighlightableText = ({ text, highlight, className }) => (
  <div
    className={classnames(className)}
    dangerouslySetInnerHTML={createMarkup(text, highlight)}
  />
);

HighlightableText.propTypes = {
  className: PropTypes.string,
  text: PropTypes.string,
  highlight: PropTypes.string,
};

HighlightableText.defaultProps = {
  className: '',
  text: '',
  highlight: '',
};

export default HighlightableText;
