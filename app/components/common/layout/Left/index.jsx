import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';

const Left = ({ children, className }) => (
  <div className={classnames('area-left', className)}>{children}</div>
);

Left.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};

Left.defaultProps = {
  children: null,
  className: '',
};

export default Left;
