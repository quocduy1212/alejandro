import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';

const Right = ({ children, className }) => (
  <div className={classnames('area-right', className)}>{children}</div>
);

Right.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};

Right.defaultProps = {
  children: null,
  className: '',
};

export default Right;
