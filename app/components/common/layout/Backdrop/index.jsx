import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const Backdrop = ({ className, children }) => {
  const containerCls = classnames(
    'fixed absolute--fill bg-black-05 z-max',
    className
  );
  return <div className={containerCls}>{children}</div>;
};

Backdrop.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
};

Backdrop.defaultProps = {
  className: '',
  children: null,
};

export default Backdrop;
