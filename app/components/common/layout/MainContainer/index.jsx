import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';

const MainContainer = ({ children, className }) => (
  <main className={classnames('main-container ph3 absolute top-0', className)}>
    {children}
  </main>
);

MainContainer.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};

MainContainer.defaultProps = {
  children: null,
  className: '',
};

export default MainContainer;
