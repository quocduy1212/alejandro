import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';

const TopBar = ({ children, className }) => (
  <nav className={classnames('top-bar z-max absolute', className)}>
    {children}
  </nav>
);

TopBar.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};

TopBar.defaultProps = {
  children: null,
  className: '',
};

export default TopBar;
