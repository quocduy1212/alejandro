import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import './style.scss';

const TextInput = props => (
  <input
    type="text"
    className={classnames('text-input ph3 pv2', props.className)}
    placeholder={props.placeholder}
    value={props.value}
    onChange={props.onChange}
  />
);

TextInput.propTypes = {
  className: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
};

TextInput.defaultProps = {
  className: '',
  placeholder: '',
  value: '',
  onChange: _.noop,
};

export default TextInput;
