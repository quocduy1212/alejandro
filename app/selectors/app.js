import _ from 'lodash';
import { createSelector } from 'reselect';
import TYPES from 'app-types';
import asyncSelector from 'app-libs/asyncSelector';

const stateApp = state => state.app || {};

const fetchUsersSelector = createSelector(stateApp, app =>
  asyncSelector(app, TYPES.ASYNC_TYPES.FETCH_USERS.path)
);

export const fetchUsersStatus = createSelector(
  fetchUsersSelector,
  ({ isLoading, isSuccess, isError }) => ({
    isLoading,
    isSuccess,
    isError,
  })
);

export const fetchUsersData = createSelector(
  fetchUsersSelector,
  ({ response }) => response || []
);

export const fetchUsersError = createSelector(
  fetchUsersSelector,
  ({ error }) => error || {}
);

const fetchConfigSelector = createSelector(stateApp, app =>
  asyncSelector(app, TYPES.ASYNC_TYPES.FETCH_CONFIG.path)
);

export const fetchConfigStatus = createSelector(
  fetchConfigSelector,
  ({ isLoading, isSuccess, isError }) => ({
    isLoading,
    isSuccess,
    isError,
  })
);

export const fetchConfigData = createSelector(
  fetchConfigSelector,
  ({ response }) => response || {}
);

export const getFilters = createSelector(stateApp, app =>
  app.get('filters').toJS()
);

export const getFilteredUsers = createSelector(
  getFilters,
  fetchUsersData,
  (filters, users) => users.filter(u => u.name.includes(filters.name))
);

export const getEmailDistribution = createSelector(fetchUsersData, users =>
  _.groupBy(users, u => u.email[0])
);

export const getUsersAddress = createSelector(fetchUsersData, users =>
  users.map(u => ({
    id: u.id,
    geo: {
      lat: parseFloat(u.address.geo.lat),
      lng: parseFloat(u.address.geo.lng),
    },
  }))
);
