import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Landing from './components/pages/Landing';
import PageNotFound from './components/pages/PageNotFound';
import Redirector from './components/pages/Redirector';
import Initializer from './components/pages/Initializer';

const Routes = () => (
  <BrowserRouter>
    <Initializer>
      <Redirector>
        <Switch>
          <Redirect exact from="/" to="/app" />
          <Route path="/app" component={Landing} />
          <Route component={PageNotFound} />
        </Switch>
      </Redirector>
    </Initializer>
  </BrowserRouter>
);

export default Routes;
