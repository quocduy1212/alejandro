export default (state, asyncPath) => {
  const asyncState = state.getIn(asyncPath).toJS() || {};
  const { response, error, type = '' } = asyncState;
  return {
    isLoading: type.endsWith('_LOADING'),
    isSuccess: type.endsWith('_SUCCESS'),
    isError: type.endsWith('_ERROR'),
    response,
    error,
  };
};
