import { MENUS } from '../constants/appMenus';

const isMenuSelected = (menu, pathname) => pathname.startsWith(menu.href);

export const createMenu = pathname =>
  MENUS.map(m => ({
    ...m,
    active: isMenuSelected(m, pathname),
  }));
