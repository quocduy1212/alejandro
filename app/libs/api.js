const USER_API_URL = 'http://jsonplaceholder.typicode.com/users';

export const fetchUsers = () =>
  fetch(USER_API_URL).then(response => response.json());

export const fetchConfig = () =>
  new Promise(resolve =>
    setTimeout(() => resolve({ authenticated: true }), 1000)
  );
