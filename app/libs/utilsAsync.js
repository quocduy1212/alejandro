import { call, put } from 'redux-saga/effects';
import { fromJS } from 'immutable';

export const createAsyncTypes = TYPES => {
  const result = {};
  const ASYNC_TYPES = {};
  TYPES.forEach(([TYPE, path]) => {
    const loading = `${TYPE}_LOADING`;
    const success = `${TYPE}_SUCCESS`;
    const error = `${TYPE}_ERROR`;

    result[TYPE] = TYPE;
    result[loading] = loading;
    result[success] = success;
    result[error] = error;

    ASYNC_TYPES[TYPE] = {
      loading,
      success,
      error,
      path,
    };
  });
  result.ASYNC_TYPES = ASYNC_TYPES;

  return result;
};

export const createAsyncHanders = ASYNC_TYPES => {
  const handlers = {};

  Object.values(ASYNC_TYPES).forEach(({ loading, success, error, path }) => {
    handlers[loading] = state => state.mergeIn(path, { type: loading });
    handlers[success] = (state, action) =>
      state.mergeIn(path, {
        type: success,
        response: fromJS(action.data),
      });

    handlers[error] = (state, action) =>
      state.mergeIn(path, {
        type: error,
        error: fromJS(action.error),
      });
  });

  return handlers;
};

export const createSagaForAsyncType = (ASYNC_TYPE, apiCall) =>
  function* asyncActionSaga() {
    try {
      yield put({ type: ASYNC_TYPE.loading });
      const data = yield call(apiCall);
      yield put({ type: ASYNC_TYPE.success, data });
    } catch (error) {
      yield put({ type: ASYNC_TYPE.error, error });
    }
  };
