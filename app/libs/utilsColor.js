import _ from 'lodash';

export const randomColor = () =>
  `rgb(${_.random(0, 255)}, ${_.random(0, 255)}, ${_.random(0, 255)})`;
