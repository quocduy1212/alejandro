import TYPES from 'app-types';

export const fetchUsers = () => ({
  type: TYPES.FETCH_USERS,
});

export const fetchConfig = () => ({
  type: TYPES.FETCH_CONFIG,
});

export const filterByName = name => ({
  type: TYPES.FILTERS,
  filters: { name },
});
