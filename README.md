# phamduy

## Demo

![Demo](/demo.gif?raw=true 'Demo')

## Install `yarn` if you don't have it yet

1. `brew update`
2. `brew install yarn`

## Run app in development mode

1. `yarn`
2. `yarn start`

It will take a while to finish first bundling.

## Build the app for production

1. `yarn build` or just `yarn`
2. outpus are in `dist/`

### Do HMR (hot module replacement) work?

Yes, app updates without browser reloading. You don't lose state!


## Decisions

1. configurated prettier for consitent formatting
2. eslint is run on re-compile
3. Webpack configurations are split to two files, one for development and one for production. Development to provide hot reload, source map, no optimization. Production to split code, extract css, JS optimization, css class name optimization.
4. Vendor JS and CSS are split to separated files
5. Don’t use redux logger middleware to encourage people use redux devtool extension
6. Use yarn instead of npm for better build performance
7. Styling component use tachyons functional classes. Tachyons provided many useful classes, like grids, spacing, typography, alignment, text processing…
8. Data convert between state and UI component are handled using reselect (selector)
9. Common paths are defined as alias in babelrc, make it easier to do absolute import instead of relative import.
10. eslint loader is run first for both development and production build config, and it has to be passed in order to bundle resources.
11. implement simple HOC for handling with router and click-outside
12. redux-saga to handle side effects

## Development tools

1. eslint: extended from airbnb
2. babel ES6 loaders: enable ES6 syntax
3. redux-devtool-extension: redux tool for debugging and managing state during development
4. postcss: transforming styles, autoprefix, mixins…
5. webpack: JS bundler, code splitting, preprocess files using loaders

